import { ChakraProvider } from '@chakra-ui/provider'
import {
	Accordion,
	AccordionButton,
	AccordionItem,
	AccordionPanel,
	Button,
	Container,
	Divider,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Modal,
	Heading,
	VStack,
	useDisclosure,
	theme,
} from '@chakra-ui/react'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import ModalQuestion from './components/ModalQuestion'
import { setRandom, setSubject } from './redux/slice/question.slice'

function App() {
	const dispatch = useDispatch()
	const { isOpen, onOpen, onClose } = useDisclosure()

	const handleAttendClick = (subject) => {
		const random = []
		for (let i = 0; i < 4; i++) {
			random.push(Math.round(Math.random() * 19) + 1)
		}
		dispatch(setRandom(random))
		dispatch(setSubject(subject))
		onOpen()
	}

	return (
		<>
			<Container maxW='container.xl'>
				<VStack>
					<Heading>QuizzLand</Heading>
					<Accordion w='100%' allowMultiple>
						<AccordionItem>
							<AccordionButton w='100%'>
								<Heading as='h6' size='md' textAlign='center'>
									Chủ đề 1
								</Heading>
							</AccordionButton>
							<AccordionPanel>
								Chủ đề 1 là những câu hỏi về thiên nhiên Lorem ipsum dolor sit
								amet consectetur adipisicing elit. Magni, natus.
								<Divider my='2' />
								Bạn đã trả lời đúng{' '}
								{parseInt(localStorage.getItem('subject1')) || 0} câu hỏi của
								chủ đề 1
								<Divider my='2' />
								<Button
									onClick={() => handleAttendClick('subject1')}
									disabled={parseInt(localStorage.getItem('subject1')) === 4}
								>
									Tham gia chủ đề 1
								</Button>
							</AccordionPanel>
						</AccordionItem>

						<AccordionItem>
							<AccordionButton w='100%'>
								<Heading as='h6' size='md' textAlign='center'>
									Chủ đề 2
								</Heading>
							</AccordionButton>
							<AccordionPanel>
								Chủ đề 2 là những câu hỏi về thiên nhiên Lorem ipsum dolor sit
								amet consectetur adipisicing elit. Magni, natus.
								<Divider my='2' />
								Bạn đã trả lời đúng{' '}
								{parseInt(localStorage.getItem('subject2')) || 0} câu hỏi của
								chủ đề 2
								<Divider my='2' />
								<Button
									onClick={() => handleAttendClick('subject2')}
									disabled={parseInt(localStorage.getItem('subject2')) === 4}
								>
									Tham gia chủ đề 2
								</Button>
							</AccordionPanel>
						</AccordionItem>
						<AccordionItem>
							<AccordionButton w='100%'>
								<Heading as='h6' size='md' textAlign='center'>
									Chủ đề 3
								</Heading>
							</AccordionButton>
							<AccordionPanel>
								Chủ đề 3 là những câu hỏi về thiên nhiên Lorem ipsum dolor sit
								amet consectetur adipisicing elit. Magni, natus.
								<Divider my='2' />
								Bạn đã trả lời đúng{' '}
								{parseInt(localStorage.getItem('subject3')) || 0} câu hỏi của
								chủ đề 3
								<Divider my='2' />
								<Button
									onClick={() => handleAttendClick('subject3')}
									disabled={parseInt(localStorage.getItem('subject3')) === 4}
								>
									Tham gia chủ đề 3
								</Button>
							</AccordionPanel>
						</AccordionItem>
						<AccordionItem>
							<AccordionButton w='100%'>
								<Heading as='h6' size='md' textAlign='center'>
									Chủ đề 4
								</Heading>
							</AccordionButton>
							<AccordionPanel>
								Chủ đề 4 là những câu hỏi về thiên nhiên Lorem ipsum dolor sit
								amet consectetur adipisicing elit. Magni, natus.
								<Divider my='2' />
								Bạn đã trả lời đúng{' '}
								{parseInt(localStorage.getItem('subject4')) || 0} câu hỏi của
								chủ đề 4
								<Divider my='2' />
								<Button
									onClick={() => handleAttendClick('subject4')}
									disabled={parseInt(localStorage.getItem('subject4')) === 4}
								>
									Tham gia chủ đề 4
								</Button>
							</AccordionPanel>
						</AccordionItem>
					</Accordion>
				</VStack>
			</Container>

			<ModalQuestion isOpen={isOpen} onClose={onClose} />
		</>
	)
}

export default App
