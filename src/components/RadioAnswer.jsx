import React from 'react'
import { useRadio, Box } from '@chakra-ui/react'

const RadioAnswer = (props) => {
	const { getInputProps, getCheckboxProps } = useRadio(props)

	const input = getInputProps()
	const checkbox = getCheckboxProps()

	return (
		<Box as='label' pointerEvents={props.pointerEvents}>
			<input {...input} />
			<Box
				{...checkbox}
				_checked={{
					bg: 'teal.600',
					color: 'white',
					borderColor: 'teal.600',
				}}
				_focus={{
					boxShadow: 'outline',
				}}
				cursor='pointer'
			>
				{props.children}
			</Box>
		</Box>
	)
}

export default RadioAnswer
