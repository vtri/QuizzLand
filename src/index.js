import { ChakraProvider } from '@chakra-ui/provider'
import { ColorModeScript } from '@chakra-ui/react'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import store from './redux/store'
import { theme } from './theme'
import { QueryClientProvider, QueryClient } from 'react-query'

const client = new QueryClient()

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<ColorModeScript />
			<ChakraProvider resetCSS theme={theme}>
				<QueryClientProvider client={client}>
					<App />
				</QueryClientProvider>
			</ChakraProvider>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
)
